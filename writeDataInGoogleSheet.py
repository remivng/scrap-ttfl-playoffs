#!/usr/bin/env python3

# Imports
import json
from datetime import date
import gspread

# Get today's date
todayTiret = date.today().strftime("%d-%m-%Y")
todaySlash = date.today().strftime("%d/%m/%Y")

# Config gsheat
gc = gspread.service_account(filename='credentials.json')
sh = gc.open_by_key('15fOTrU9jFCZDmhZaemcuaxz-JcMVtFppJ-zuI7QgdS0')

# Read file
fileName = "./json/" + todayTiret + ".json"
with open(fileName, 'r') as myFile:
    jsonData = json.load(myFile)

# Constants
colDatesIndex = 2
colPlayersBeginning = 3
colPlayersEnd = 12

teamsList = jsonData["teams"]

# Iterate for each team
for i, team in enumerate(teamsList):

    # Save Team data
    teamName = team["team_name"]
    listPlayers = team["players"]

    # LOG DEMO
    print("Récupération des données pour l'équipe " + teamName + "...")

    # Get Team Worksheet
    worksheet = sh.worksheet(teamName)

    # Find col with corresponding date
    colsList = worksheet.col_values(colDatesIndex)

    # Dumb value if date not found
    rowIndex = 1000
    if todaySlash in colsList:
        rowIndex = colsList.index(todaySlash) + 1

    cells = worksheet.range(rowIndex, colPlayersBeginning, rowIndex, colPlayersEnd)

    # Iterate for each player
    for j, player in enumerate(listPlayers):
        # Get values from player
        playerName = player["player_name"]
        pick = player["pick"]
        cells[j].value = pick

    # LOG DEMO
    print("Ecriture des données dans le fichier Excel pour l'équipe " + teamName + "...")
    worksheet.update_cells(cells)

print("Terminé.")
