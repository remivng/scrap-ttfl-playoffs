#!/usr/bin/env python3

# Imports
import json
import re
import requests
from bs4 import BeautifulSoup
import numpy as np
from datetime import date

# Api end-point
URL_GET_TEAM = "https://fantasy.trashtalk.co/?tpl=equipe&team="

# Cookies remivng
cookies_dict = {"PHPSESSID": "1faivgmdj5aglmpd2k2gh03355"}

# Teams name array
TEAMS = np.array([
                    "Les+genie", "NeverBrick", "UU+TEAM", "AI+Factory",
                    "franckyVTe", "TTFL_GOAT", "DKcito59", "NoBrainers",
                    "MeuteWorld", '24"Bons', "DrAkar", "TeamDatass",
                    "Garbage+T", "LesMarlous", "Joke+City", "GreenCity"
                ])

# Teams name array for testing
# TEAMS = np.array(["DKcito59"])

# Init JSON file
fullJson = {"teams": []}

# Loop for each 16 team
for i, teamName in enumerate(TEAMS):

    # LOG DEMO
    print("Récupération des picks pour la team " + teamName + "en cours...")

    # Sending GET request
    url = URL_GET_TEAM + teamName
    team_result = requests.get(url=url, cookies=cookies_dict)
    data = team_result.content

    # Init JSON for team
    team_json = {
                    "team_name": teamName,
                    "players": []
                }

    # Parse HTML result
    soup = BeautifulSoup(data, 'html.parser')
    tr_list = soup.find('table', id="classementTeamTabme").find('tbody').find_all('tr')

    # Loop to find the 10 picks of the team
    for j, player in enumerate(tr_list):

        # Get first player infos
        player_name = player.find('b').find('a').get_text()
        score = player("td")[-1].get_text()

        # Format text (remove parenthesis and whitespaces)
        player_name = player_name.replace(u'\xa0', u'')
        score = re.sub(r"\([^()]*\)", "", score).strip()

        # Set infos as Json
        player_json = {
                        "player_name": player_name,
                        "pick": score
                    }

        # Add player json to team json
        team_json["players"].append(player_json)

    # Sort team list by players name (if ranking change)
    sortedTeamJson = sorted(team_json["players"], key=lambda x: x["player_name"].lower(), reverse=False)
    team_json["players"] = sortedTeamJson

    # LOG DEMO
    print("Sauvegarde des picks pour la team " + teamName + " en cours...")

    # Then, add the team json to the full json
    fullJson["teams"].append(team_json)

# Get today's date
today = date.today().strftime("%d-%m-%Y")

# Save json as file
fileName = "./json/" + today + ".json"
with open(fileName, 'w') as outfile:
    json.dump(fullJson, outfile)

# LOG DEMO
print("Toutes les données ont été sauvegardées dans le fichier " + fileName + "!")
